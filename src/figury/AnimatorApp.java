package figury;

import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentAdapter;

public class AnimatorApp extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private AnimPanel kanwa;
	private JButton dodaj;
	private JButton animacja;

	/**
	 * Create the frame.
	 * @param// delay
	 */
	public AnimatorApp() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int ww = 450, wh = 300;
		setBounds((screen.width-ww)/2, (screen.height-wh)/2, ww, wh);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		kanwa = new AnimPanel();
		kanwa.addComponentListener(kanwa.new PanelResizeListener());
		setBackground(Color.WHITE);
		kanwa.setBounds(10,11,420,220);
		contentPane.add(kanwa);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				kanwa.initialize();
			}
		});


		dodaj = new JButton("Dodaj");
		dodaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kanwa.addFig();
			}
		});
		dodaj.setBounds(10, 239, 90, 20);
		contentPane.add(dodaj);

		animacja = new JButton("Animuj");
		animacja.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kanwa.animate();
			}
		});
		animacja.setBounds(100, 239, 90, 20);
		contentPane.add(animacja);

	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					final AnimatorApp frame = new AnimatorApp();
					frame.addComponentListener(frame.new Skalowanie());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Launch the application.
	 */
	class Skalowanie extends ComponentAdapter{
		public void componentResized(ComponentEvent e) {
			kanwa.setBounds(10, 11, e.getComponent().getWidth() - 30, e.getComponent().getHeight() - 80);
			dodaj.setBounds(10, e.getComponent().getHeight() - 65, 80, 23);
			animacja.setBounds(100, e.getComponent().getHeight() - 65, 80, 23);
		}
	}

}
