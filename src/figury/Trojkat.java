package figury;

import java.awt.geom.*;
import java.awt.*;

public class Trojkat extends Figura {
    public Trojkat(Graphics2D buf, int del, int w, int h) {
        super(buf, del, w, h);
        shape = new Polygon(new int[]{0, 55, 20}, new int[]{0, 0, 45}, 3);

        aft = new AffineTransform();
        area = new Area(shape);
    }

}
