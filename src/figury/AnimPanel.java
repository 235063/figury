package figury;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.Timer;

public class AnimPanel extends JPanel implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Figura> listaFigur = new LinkedList<Figura>();

	// bufor
	Image image;
	// wykreslacz ekranowy
	Graphics2D device;
	// wykreslacz bufora
	Graphics2D buffer;

	private int delay = 40;

	private Timer timer;

	private static int numer = 0;

	public AnimPanel() {
		super();
		setBackground(Color.WHITE);
		timer = new Timer(delay, this);
	}

		void addFig() {
			Figura fig = null;
			if(numer % 4 == 0) {
				fig =  new Kwadrat(buffer, delay, getWidth(), getHeight());
			}
			if(numer % 4 == 1) {
				fig =  new Elipsa(buffer, delay, getWidth(), getHeight());
			}
			if(numer % 4 == 2) {
				fig =  new Trojkat(buffer, delay, getWidth(), getHeight());
			}
			if(numer % 4 == 3) {
				fig =  new Prostokat(buffer, delay, getWidth(), getHeight());
			}
			numer++;
			timer.addActionListener(fig);
			listaFigur.add(fig);
			new Thread(fig).start();
		}

	public void initialize() {
		int width = getWidth();
		int height = getHeight();

		image = createImage(width, height);
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	}

	void animate() {
		if (timer.isRunning()) {
			timer.stop();
			for(Figura fig : listaFigur){
				fig.stopowanie();
			}
		} else {
			timer.start();
			for(Figura fig : listaFigur) {
				fig.startowanie();
			}
		}
	}

	class PanelResizeListener extends ComponentAdapter {        //skalowanie animacji - resetuje ca³oœæ
		public void componentResized(ComponentEvent e) {
			image = createImage(e.getComponent().getWidth(), e.getComponent().getHeight());
			buffer = (Graphics2D) image.getGraphics();
			buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			device = (Graphics2D) getGraphics();
			device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		device.drawImage(image, 0, 0, null);
		buffer.clearRect(0, 0, getWidth(), getHeight());
	}
}
