package figury;

import java.awt.geom.*;
import java.awt.Graphics2D;

public class Prostokat extends Figura {

    public Prostokat(Graphics2D buf, int del, int w, int h){
        super(buf,del,w,h);
        shape = new Rectangle2D.Float(0,0,25,15);
        aft = new AffineTransform();
        area = new Area(shape);
    }
}
