package figury;

import java.awt.geom.*;
import java.awt.Graphics2D;

public class Elipsa extends Figura {
    public Elipsa(Graphics2D buf, int del, int w, int h) {
        super(buf, del, w, h);
        shape = new Ellipse2D.Double(0,0,20,17);
        aft = new AffineTransform();
        area = new Area(shape);

    }
}
